let printing_text_rows = new Array();
printing_text_rows.push({static_text: "Login: ", printing_text: "dimkakom"});
printing_text_rows.push({static_text: "Password: ", printing_text: "*********"});

var fashingBorderInterval = 0.75;
var logingAreaAnimationDuration = 0;
printing_text_rows.forEach((item, index) => {
    var printingCharactersLength = String(item.value).length;
    var printingStartDelay = 1000;
    var textPrintingDuration = printingCharactersLength * fashingBorderInterval * 300;    
    logingAreaAnimationDuration += textPrintingDuration + printingStartDelay;
    setTimeout(function() {
        var row = document.createElement("tr"); // Adding new table row
        // Creating table column for key text
        var key_column = document.createElement("td"); 
        key_column.textContent = item.static_text;
        key_column.className = "static-text";
        key_column.style.width = `${printingCharactersLength}ch`;
        // Creating column for value text (will have printing animation)
        var value_column = document.createElement("td");    
        var printingText = document.createElement("div");
        printingText.id = `line-${index}`;
        printingText.textContent = `${item.printing_text}`;
        printingText.className = "text-printing";
        printingText.style.width = `0%`;
        printingText.style.animation = `flashin-border ${fashingBorderInterval}s step-start infinite`;
        value_column.appendChild(printingText);
        row.appendChild(key_column);
        row.appendChild(value_column);
        setTimeout(function() {
            printingText.style.width = `${printingCharactersLength}ch`;
            printingText.style.animation += `,printed-text ${textPrintingDuration}ms steps(${printingCharactersLength})`;
        }, printingStartDelay);
        document.getElementById("loginTextBox").appendChild(row);
        // Disable border flashing
        setTimeout(function() {
            printingText.style.borderColor = 'transparent';
            printingText.style.animation = "";
        }, printingStartDelay + textPrintingDuration);
    }, index * (textPrintingDuration + printingStartDelay));
});

var body = document.body;
var html = document.documentElement;

var body_height = Math.max( body.scrollHeight, body.offsetHeight, 
                       html.clientHeight, html.scrollHeight, html.offsetHeight );

var console_area_height = body_height * 0.4;
var max_console_rows = Math.floor(console_area_height / 21);
let console_loading_rows = new Array();

var connection_animation_duration = 1000;

console_loading_rows.push('Establishing a secure connection...');
console_loading_rows.push('Connection successful!');
console_loading_rows.push('Checking login and password...');
console_loading_rows.push('Login and password are valid!');
console_loading_rows.push('Downloading personal data...');
console_loading_rows.push('Client data successfully downloaded!');
console_loading_rows.push('Encripting data...');
console_loading_rows.push('Data encripted successful!');
console_loading_rows.push('Extracting data...');
console_loading_rows.push('Data extracted successful!');
console_loading_rows.push('Loading data displaying...');
console_loading_rows.push(' ');
console_loading_rows.push(' ');

var loging_and_conection_animation_duration = logingAreaAnimationDuration + connection_animation_duration * console_loading_rows.length;

let client_information_rows = new Array();
client_information_rows.push('Client information:');
client_information_rows.push(' ');
client_information_rows.push('First name: Dmytro');
client_information_rows.push('Second name: korostashov');
client_information_rows.push('Birthday: 27.04.1994');
client_information_rows.push('Email: jiotiata@gmail.com');
client_information_rows.push('Skype: live:jiotiata');
client_information_rows.push('LinkedIn: dima-kom-2b48511b3');
client_information_rows.push('Trade: software developming, electronics and telecommunications engineering');
client_information_rows.push('Programing languages:');
client_information_rows.push('  > C#');
client_information_rows.push('  > TypeScript');
client_information_rows.push('  > HTML, CSS#');
client_information_rows.push('Main technology stack: ');
client_information_rows.push('  > .NET Framework since 2016');
client_information_rows.push('  > .NET Core, Angular platforms since the end of 2019');
client_information_rows.push('  > DBMS: MSSQL, Entity Framework Core since 2019');
client_information_rows.push('More information here: <a href="dmytro_korostashov_cv.docx" download="dmytro_korostashov_cv.docx">dimkakom_CV.docx</a>');
client_information_rows.push('End of data...');

setTimeout(function(){
    console_loading_rows.forEach((item, index) => {
        setTimeout(function() {
            var row = document.createElement("tr"); // Adding new table row
            row.style.height = 18;
            row.id = `console-row-${index}`;
            // // Creating table column for  text
            var column = document.createElement("td");  
            var content = document.createElement("div");
            content.textContent = item;
            content.style.width = `${String(item).length}ch`;
            content.className = "text-printing";
            content.style.animation = `flashin-border ${fashingBorderInterval}s step-start infinite`;
            column.appendChild(content);
            row.appendChild(column);
            // Deleting redundant lines
            document.getElementById("consoleOutputTextBox").appendChild(row);
            if (index > max_console_rows) {
                var element = document.getElementById(`console-row-${index - max_console_rows}`);
                element.parentNode.removeChild(element);
            }
            // Disable border flashing
            setTimeout(function() {
                content.style.borderColor = 'transparent';
                content.style.animation = "";
            }, connection_animation_duration);
        }, index * connection_animation_duration);
    });
},logingAreaAnimationDuration);

setTimeout(() => {
    client_information_rows.forEach((item, index) => {
        setTimeout(function() {
            var row = document.createElement("tr"); // Adding new table row
            row.style.height = 18;
            row.id = `console-row-${console_loading_rows.length + index}`;
            // // Creating table column for key text
            var column = document.createElement("td");  
            var content = document.createElement("div");
            content.className = "static-text";
            if (item.includes('<') && item.includes('>')) {
                content.innerHTML = item;
            } else {
                content.textContent = item;                
            }
            column.appendChild(content);
            row.appendChild(column);
            document.getElementById("consoleOutputTextBox").appendChild(row);
            // Deleting redundant lines
            if (console_loading_rows.length + index > max_console_rows) {
                var element = document.getElementById(`console-row-${console_loading_rows.length - 1 + index - max_console_rows}`);
                element.parentNode.removeChild(element);
            }
            if (index == client_information_rows.length - 1) {
                content.style.width = `${String(item).length}ch`;
                content.className = "text-printing";
                content.style.animation = `flashin-border ${fashingBorderInterval}s step-start infinite`;
            }
        }, index * 100);
    });
},loging_and_conection_animation_duration);
